import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import colorAxis from 'highcharts/modules/coloraxis';
import * as L from 'leaflet';
import {
  faHome,
  faPaperPlane,
  faFolder,
  faCog,
  faUsers,
  faSitemap
} from '@fortawesome/free-solid-svg-icons';

const COORDENADAS = [
  [41.66, -4.71, 'Laguna del duero', 'Campo de antenas', '2', '34', '27'],
  [42.25009962050302, -6.024332214781339, 'La beñeza', 'Emisión paper view', '2', '34', '27'],
  [37.27102320102085, -4.574137020066445, 'Benameji', 'Emisor internacional', '2', '34', '27'],
  [38.467812895777804, -6.46378530408888, 'Zafra', 'Receptor europa', '2', '34', '27'],
  [42.00565996204903, -6.156168141573601, 'Valladolid', 'Emision directo', '2', '34', '27'],
  [41.382195351590724, -0.15763347252563956, 'Caspe', 'Campo de antenas', '2', '34', '27'],
  [39.067423252460266, -1.6737466306366606, 'Chinchilla', 'Albacete visión', '2', '34', '27'],
  [40.03311149205948, -6.639566539811897, 'Moraleja', 'Emisión peninsula', '2', '34', '27'],
  [43.15433141010707, -8.573160132765086, 'La Coruña', 'Emisión GaliciaTV', '2', '34', '27'],
  [40.049933282872416, -3.101969170886178, 'Tarancón', 'Radio Madrid', '2', '34', '27'],
  [41.068198788338734, -1.805582557428926, 'Fuentelsaz', 'Teruel sur', '2', '34', '27'],
  [36.391692081653815, -5.738687706731435, 'Medina sodonia', 'Campo de antenas', '2', '34', '27'],
  [35.21312636390038, -2.9743750307539973, 'Melilla', 'Radio Melilla', '2', '34', '27'],
  [40.84210317467072, -3.2900286180393747, 'Matarrubia', 'Tele Madrid', '2', '34', '27'],
  [40.43360303319105, -3.0795929623438973, 'Escariche', 'Campo de antenas', '2', '34', '27'],
  [40.92535194891174, -5.208418566154919, 'Peñaranda', 'Radio salamanca', '2', '34', '27']
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'sargi-angular';
  faHome = faHome;
  faPaperPlane = faPaperPlane
  faFolder = faFolder
  faCog = faCog;
  faUsers = faUsers;
  faSitemap = faSitemap;
  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
        text: 'Alarmas por categoría'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    colorAxis: {
        minColor: '#FF0000',
        maxColor: '#002DFE'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Alarmas',
        type: 'pie',
        colorByPoint: true,
        data: [{
            name: 'No urgente',
            y: 66.9,
            sliced: true,
            selected: true
        }, {
            name: 'Semi urgente',
            y: 30
        }, {
            name: 'Urgente',
            y: 3.1
        }]
    }],
    credits: {
      enabled: false
    }
  };
  chartOptions2: Highcharts.Options = {
    chart: {
        type: 'area'
    },
    title: {
      text: 'Potencia Contratada - Consumo del centro'
    },
    xAxis: {
      categories: ['Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre', 'Enero'],
      title: {
          text: 'Mes'
      }
    },
    yAxis: {
      labels: {
          formatter() {
              return this.value / 10000 + 'k';
          }
      }
    },
    tooltip: {
      pointFormat: '{series.name} <b>{point.y:,.0f}</b><br/>'
    },
    series: [{
        type: 'area',
        name: 'Potencia contratada',
        data: [24237, 24237, 24401, 24344, 23586,
            22380, 21004, 17287, 18747,
            19871, 19824, 19577
        ]
      },
      {
        name: 'Consumo',
        type: 'area',
        data: [
            5, 25, 50,
            4238, 5221, 6129,
            15915, 17385, 19055,
            33952, 22237,
            18000
        ]
      }
    ],
    credits: {
      enabled: false
    }
  };
  chartOptions3: Highcharts.Options = {
    title: {
      text: 'Histórico de temperaturas medias'
    },
    xAxis: {
        categories: ['Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre', 'Enero'],
        title: {
            text: 'Mes'
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Grados'
        }
    },
    colorAxis: {
        minColor: '#001FFE',
        maxColor: '#FF0000'
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    series: [{
        type: 'column',
        data: [
            ['Febrero', 18],
            ['Marzo', 16],
            ['Abril', 20],
            ['Mayo', 23],
            ['Junio', 24],
            ['Julio', 28],
            ['Agosto', 32],
            ['Septiembre', 33],
            ['Octubre', 32],
            ['Noviembre', 27],
            ['Dciembre', 22],
            ['Enero', 19]
        ],
        name: 'Grados'
    }],
    credits: {
      enabled: false
    }
  };

  private map;

  constructor() {}

  ngOnInit() {
    colorAxis(this.Highcharts);
    const myMap = L.map('map').setView([40.066750923220724, 0], 6);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(myMap);

    const marcadores: any[] = [];
    for (let i = 0; i < COORDENADAS.length; i++) {
      const url = encodeURI('app_todo4.html?centro=' + COORDENADAS[i][2].toString() + '&descripcion=' + COORDENADAS[i][3].toString() + '&alarmas=' + COORDENADAS[i][4].toString() + '  &consumo=' + COORDENADAS[i][5].toString() + '&temperatura=' + COORDENADAS[i][6].toString());
      marcadores[i] = L.marker([COORDENADAS[i][0],
        COORDENADAS[i][1]
      ], {
          draggable: false
      }).addTo(myMap).bindPopup(
          `Descripcion: ${COORDENADAS[i][2]}<br>
          Alarmas activas: ${COORDENADAS[i][3]}<br>
          Consumo: ${COORDENADAS[i][4]} W<br>
          Temperatura: ${COORDENADAS[i][6]} &#8451;<br>
          <a href="${url}"><i class="fa fa-gear"></i> Ver dashboard</a>
          <a class="enlace-propio" href="./eventos.html?centro='${COORDENADAS[i][2]}"><i class="fa fa-bell"></i>Ver eventos</a><br>`
      );
      marcadores[i].nombre = COORDENADAS[i][2];
      marcadores[i].descripcion = COORDENADAS[i][3];
      marcadores[i].alarmasActivas = COORDENADAS[i][4];
      marcadores[i].consumo = COORDENADAS[i][5];
      marcadores[i].temperatura = COORDENADAS[i][6];
      if (i % 2 === 0) {
          marcadores[i]._icon.src = './assets/img/marker-icon-2x-orange.png';
          marcadores[i].color = '#CB8427';
      } else if (i % 3 === 0) {
          marcadores[i]._icon.src = './assets/img/marker-icon-2x-red.png';
          marcadores[i].color = '#CB2B3E';
      } else {
          marcadores[i].color = '#2A81CB';
      }
    }
  }
}
